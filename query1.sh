#!/bin/bash

#https://docs.microsoft.com/en-us/cli/azure/ext/log-analytics/monitor/log-analytics?view=azure-cli-latest

#az login
#az account set --subscription 27b2a9c9-89f6-44cf-b4ec-74df29dce1fd

#az monitor log-analytics query -w 39839539-428a-4abd-a775-3727681828a4 --analytics-query "AzureActivity | summarize count() by bin(timestamp, 1h)" -t P3DT12H
todatetime1="$1" # 2020-10-16T12:07:00
todatetime2="$2" # 2020-10-19T09:47:00

#az monitor log-analytics query --workspace 39839539-428a-4abd-a775-3727681828a4 --analytics-query "ContainerLog | where TimeGenerated > todatetime('2020-10-16T12:07:00') | where TimeGenerated <= todatetime('2020-10-19T09:47:00') | extend app = extractjson('$.app', LogEntry) | extend type = extractjson('$.type', LogEntry) | where type == 'AUDIT'and app startswith'pex360' | project LogEntry | count" #-t P3DT12H
az monitor log-analytics query --workspace 39839539-428a-4abd-a775-3727681828a4 --analytics-query "ContainerLog | where TimeGenerated > todatetime('$todatetime1') | where TimeGenerated <= todatetime('$todatetime2') | extend app = extractjson('$.app', LogEntry) | extend type = extractjson('$.type', LogEntry) | where type == 'AUDIT'and app startswith'pex360' | project LogEntry | count" -o tsv #-t P3DT12H

#az monitor log-analytics query --analytics-query
#                               --workspace
#                               [--timespan]
#                               [--workspaces]